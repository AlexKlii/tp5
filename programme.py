def factorielle(n: int) -> int:
    return 1 if (n == 0) else (n * factorielle(n-1))

with open('./data.txt', 'r') as f:
    data = f.read()

    for i in data.splitlines():
        print(factorielle(int(i)))